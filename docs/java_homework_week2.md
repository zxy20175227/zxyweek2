# 20175227 2018-2019-2 《Java程序设计》第二周学习总结

## 教材学习内容总结

1、	根据蓝墨云上的学习视频，自学第二、三章知识，并自行编译调试书上程序。
2、	第二章主要内容：标识符和关键字；基本数据类型；类型转换运算；输入、输出数据；数组。
3、	第三章主要内容：运算符与表达式；语句概述；if条件分支语句；switch开关语句；循环语句；break和continue语句；数组与for语句。

特别注意：
一、	标识符。
1、	由字母（此处字母是Unicode字符集中所含包括中文、日文片假名、平假名等许多语言中的文字）、下划线、美元符号和数字组成；
2、	第一个字符不能是数字字符；
3、	标识符不能是关键字，并且需与文件名保持一致。

二、	浮点类型。
1、	byte的取值范围是[-128，127]，若超出该范围则按顺序127、-128、-127……127取值。
2、	类型强转时，可能会导致精度缺失，如书P22-例子2。
三、	数组。
1、	声明数组时：
int [] a,b等价于int a[],b[]（声明a、b两个一维数组）；
int []a,b[]等价于int a[],b[][]（声明一个一维数组a和二维数组b）。
2、	书P28
对于数字：
int a[]={1,2,3,4};
System.out.println(""+a);——输出a的引用（首地址）。
System.out.println(""+a[0]);——输出a[0]的值。
对于字符串：
char a[]={‘中’,’国’,’科’,’大’};
System.out.println(""+a);——输出a的引用。
System.out.println(a);——输出a中全部元素的值。

四、	for语句与数组：for(声明循环变量:数组的名字);
int a[]={1,2,3,4};
for(int n=0;n<a.length;n++){
        System.out.println(a[n]);
}//传统办法
    for(int i:a){
      System.out.println(i);
}//int i与a的类型必须一致

五、	Scanner类。
Scanner reader=new Scanner(System.in);
while(reader.hasNextDouble()){
      double x=reader.nextDouble();//此时reader.nextDouble()为用户在终端输入数字回车确认输入；reader.hasNextDouble()为真假判别，是循环体控制条件。
      m=m+1;
      sum=sum+x;
    }

## 教材学习中的问题和解决过程

- 问题1：在编写程序时，System.out.println(""+a);的“+”漏写导致程序编译失败。
- 问题1解决方案：加上“+”即可。
- 问题２：在尝试“git push”时，上传失败。
 ![](https://images.gitee.com/uploads/images/2019/0307/211411_121cc525_4790138.jpeg "6.jpg")
- 问题２解决方案：后来通过百度找到解决办法——输入“push –u origin +master”将权限添加后，成功上传至码云。

## [代码托管] https://gitee.com/zxy20175227/zxyweek2
![](https://images.gitee.com/uploads/images/2019/0308/172346_b30909d3_4790138.jpeg)
![](https://images.gitee.com/uploads/images/2019/0308/172346_20e96a70_4790138.jpeg)